# java-jar-server

This is a docker image for running any java server within an executable jar file, e.g. a springboot application.

### Prerequisites

* docker
* a brief understanding on docker containers
* optional but recommended docker-compose

## Getting Started

### Installing

Write either a dockerfile which has this as base image or a docker-compose file. Since I am orchestrating my services with docker-compose, I will explain this approach.

docker-compose.yml
```yaml
version: '3'
services:
  server:
    image: mmprivat/java-jar-server
    volumes:
      - ./data/server/:/jar # mount your jar file to /jar and it will start
  environment:
      RECOMPILE: "true"
      jar: "/path/to/jarfile.jar"
```

The environment block is optional, the jar file will be searched for in /jar by default. Be aware that you cannot have multiple jar files in that dir since the lookup will just take the first it finds.

RECOMPILE (experimental) recompiles with maven before startup.

### Running

Starting the service:

```bash
# sudo docker-compose up -d
```

Stopping the service:

```bash
# sudo docker-compose down
```

### Extending

You can use this as base image and build your own images upon it when creating a `dockerfile` like this:

```bash
FROM mmprivat/java-jar-server
LABEL "MAINTAINER" "Your Name <your.name@your-email.com>"
WORKDIR /jar

# place "whatever you need to setup your container" here

ENTRYPOINT ["/scripts/start.sh"]
```

## Built With

* [Docker](http://www.docker.io/) - The container base
* [Docker-Compose](https://docs.docker.com/compose/) - Composing multiple containers into one service
* [VS Code](https://code.visualstudio.com/) - Used to edit all the files

## Contributing

If you want to contribute, contact the author or create a pull request.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Manuel Manhart** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Open issues

* Document dockers healthcheck feature
* Ignore *-javadoc.jar files

# Further information

[1] https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html
