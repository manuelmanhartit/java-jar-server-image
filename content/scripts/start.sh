#!/bin/bash

# start.sh v1.0

# This is a generic startup script for handling of docker containers. 
# It is static for all containers and provides hooks (via seperate scripts) which you would want to implement specifically for your container

# When you copied the script, create following files in the SCRIPT_PATH:
#   startService.sh - contains the service startup call (neccessary)
#   preStartup.sh   - contains any setup before the service can be started (optionally)
#   postStartup.sh  - contains any setup after the service has been started (optionally)
#   shutdown.sh     - contains cleanup if container is shutting down (optionally)

# Do not change functions unless you know what you are doing
# Normally you would want to just copy & paste this to the next container

function startupContainer() {

	# define constants
	if [ -z "$LOGFILE" ]; then
		export LOG=/var/log/container.log
	else
		export LOG=$LOGFILE
	fi

	if [ -z "$SCRIPT_PATH" ]; then
		SCRIPT_PATH=/scripts/
	fi

	if [ -f /IMAGE_NAME ]; then
		NAME=`cat /IMAGE_NAME`
	else
		NAME='unknown'
	fi
	if [ -f /IMAGE_VERSION ]; then
		VERSION=`cat /IMAGE_VERSION`
	else
		VERSION='latest'
	fi

	# run startup
	if [ -f $SCRIPT_PATH/preStartup.sh ]; then
		echo "Executing pre startup setup script" >>$LOG
		$SCRIPT_PATH/preStartup.sh
	fi

	echo "Starting service $NAME:$VERSION" >>$LOG
	$SCRIPT_PATH/startService.sh

	if [ -f $SCRIPT_PATH/postStartup.sh ]; then
		echo "Executing post startup setup script" >>$LOG
		$SCRIPT_PATH/postStartup.sh
	fi

	echo "Startup completed" >>$LOG
}

function registerSigtermHandler() {
	# Setup signal handlers
	trap 'termHandler' SIGTERM
}

function termHandler() {
	echo "Stopping container"
	if [ -f $SCRIPT_PATH/shutdown.sh ]; then
		echo "Executing shutdown script" >>$LOG
		$SCRIPT_PATH/shutdown.sh
	fi
	echo "Shutdown finished"

	exit 0
}

function runLogging() {
	while true
	do
		#sleep 1000 - Doesn't work with sleep. Not sure why.
		tail -f $1 & wait ${!}
	done
}

function main() {
	# register the sigterm handler and do all neccessary steps to shutdown the container
	registerSigtermHandler
	# do all neccessery steps to startup the container
	startupContainer
	# this is needed for the container not to stop, it will respect SIGTERM
	runLogging $LOG
}

main