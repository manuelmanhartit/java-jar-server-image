#!/bin/bash

DEFAULT_BUILD_DIR=./target

# run main service

# lookup if a jar file exists somewhere
jar=`ls . | grep .jar$`
#echo "jar: $jar"
if [ -z "$jar" ] && [ -d "$DEFAULT_BUILD_DIR" ]; then
	jar=`ls $DEFAULT_BUILD_DIR | grep .jar$`
	#echo "jar file: $jar"
fi

if [ "$PROJECT_DIR" != "" ]; then
	cd $PROJECT_DIR
	if [ "$RECOMPILE" != "" ]; then
		mvn clean package
	fi
	/usr/bin/java -jar target/*.jar
elif [ "$jar" != "" ]; then
	/usr/bin/java -jar *.jar
fi
